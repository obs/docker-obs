all: build

%.list:
	cp $(notdir $@) $@

targets = obs-server obs-api worker

build: $(targets:%=build-%)

build-%: %/stretch-backports.list
	@make -C $*
