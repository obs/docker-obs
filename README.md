== Required packages ==

* debootstrap
* docker-compose
* docker.io
* make
* osc (optional, can be on another machine)

== Build from scratch: ==

1. Debian Stretch Docker Images
   The recipes are prepared with the stock docker image for Debian Stretch.
   The build process should automatically pull the latest official Debian Stretch
   Slim variant of the image.

2. Access Privileges
   Access privileges are required for docker operations. On a Debian based system,
   the running user for docker should be added to the `docker` system group.

   ```
   $ sudo adduser $USER docker
   ```

3. Build images for OBS server and OBS api to run on different containers
   - OBS server container image
   - OBS api container image
   - OBS worker container image

   To build that images and launch in multiple containers with compose, execute:

   ```
   $ make
   ```
== Run containers ==

1. Run the containers (local developer mode)
   ```
   $ docker-compose up
   $ docker-compose scale worker=2
   ```

2. Run the containers (production mode with images from CI Container Registry)
   ```
   $ docker-compose -f docker-compose.yml -f docker-compose.prod.yml up
   $ docker-compose scale worker=2
   ```

== Access Open Build Service UI ==

1. Open Build Service should be now running, point browser to
   `https://localhost/`

For privileged access, use the `Admin` user. Its password is `opensuse`
by default. Change that password immediately.

For unprivileged access, create another user.

== Troubleshooting ==

1. docker compose fails to start
   See https://github.com/docker/compose/issues/1113
   Some cleanup might be needed if you are doing tests.
   $ docker ps -a
   $ docker rm CONTAINER_ID (listed by previous command)

== obs-tests/ ==

1. The obs-tests/ directory contains documentations, images and scripts
for testing. Please read more details in obs-tests/README.md.

As a primer on Docker:
======================

* build it
`docker build --tag my-image-name:tag .`

* run it
`docker run --name my-container-name -d my-image-name:tag`

* check the logs
`docker logs my-container-name`

* list running containers
`docker ps`

* list all containers
`docker ps -a`

more info on docker commands:
* https://github.com/wsargent/docker-cheat-sheet
