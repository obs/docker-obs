== Debian sid images for tests ==

The folder contains sid images for tests. These are a sid/minbase,
sid/obs-api, sid/obs-server and sid/worker images.

Required packages:

* debootstrap
* docker-compose
* docker.io
* make
* osc (optional, can be on another machine)
* sudo

You'll need a sid/minbase container, which you can generate with:

```
 $ sudo make minbase
```

To build the test images and launch in multiple containers with compose,
execute following commands under `docker-obs/obs-tests/sid` directory:

```
 $ sudo make
```

== Run containers ==

1. Run the containers
```
 $ sudo docker-compose up
```

== Apply the latest built OBS binaries ==

For test latest OBS that build from your ccu OBS homedir.
You may use `osc getbinaries PROJECT PACKAGE REPOSITORY ARCHITECTURE`
to fetch binaries under /tmp:
eg:
1. Switch to /tmp
```
 $ cd /tmp
```
2. Fetch binaries from your OBS homedir
```
 $ osc getbinaries home:YOUR:stretch open-build-service Debian_Stretch_backports_main x86_64
```
3. Execute script to update the latest OBS binares fetched under /tmp inside the containers
```
 $ cd -
 $ sudo ./update-obs-binaries.sh
```

== Access Open Build Service UI ==

1. Open Build Service should be now running, point browser to
   `https://localhost/`

For privileged access, use the `Admin` user. Its password is `opensuse`
by default.

For unprivileged access, create another user.

== Access to the shell of the containers ==

The worker container:
```
 docker exec -it sid_worker_1 /bin/bash
```
The obs-api container:
```
 docker exec -it sid_obs-api_1 /bin/bash
```
The obs-server container:
```
 docker exec -it sid_obs-server_1 /bin/bash
```
OBS logs are localted under /var/log/obs/ inside the containers.

== test_dod_1.sh ==

`test_dod_1.sh` (in the `obs-tests` directory) is designed to test
download-on-demand setup by building the `hello` package from Debian.

Before running it, make sure you have a `deb-src` line in your
`/etc/apt/sources.list`, and have run `apt-get update`.

You may test it by add following line into your /etc/hosts
```
 127.0.0.1	obs-api
```
and then run
```
 ./test_dod_1.sh obs-api
```
The script will setup debian projects and DoD automaticlly and submit
`hello` package build. You may check the webUI again once you ran the
script.

Use the `Admin` user. Its password is `opensuse` by default.

== Stop the containers ==
Press Ctrl-C in the console that runs the containers.

== Restart the tests from scratch ==
Stop the containers first. And then:
```
 $ sudo make clean
 $ sudo make
```
