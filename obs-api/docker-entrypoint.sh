#!/bin/sh

# Make sure there are no stale files from previous runs
rm -rfv /run/*
mkdir -m a=rwxt /run/lock

if [ -z "$DB_HOST" -o -z "$DB_ROOT_PASSWORD" -o -z "$DB_NAME" -o -z "$DB_USER" -o -z "$DB_PASSWORD" -o -z "$OBS_BACKEND_HOST" ]; then
    echo >&2 'error: database is uninitialized and password option is not specified or OBS'
    echo >&2 'error: server backend is unavailable and hostname option is not specified '
    echo >&2 '  You need to specify DB_HOST, DB_ROOT_PASSWORD, DB_NAME, DB_USER, DB_PASSWORD and OBS_BACKEND_HOST'
    exit 1
fi

# Generate ssl-cert
make-ssl-cert generate-default-snakeoil --force-overwrite
# Place api and repo url on index page
FQHOSTNAME=`hostname`
sed -e "s,___API_URL___,https://$FQHOSTNAME,g" \
    -e "s,___REPO_URL___,http://$FQHOSTNAME:82,g" \
    /usr/share/obs/overview/overview.html.TEMPLATE > \
    /usr/share/obs/overview/index.html

# Remember database passwords permanently in debconf?
echo "dbconfig-common dbconfig-common/remember-admin-pass     boolean true" | debconf-set-selections
# Configure database for  with dbconfig-common?
echo "dbconfig-common dbconfig-common/dbconfig-install boolean true" | debconf-set-selections
# Configure database for obs-api with dbconfig-common?
echo "obs-api obs-api/dbconfig-install boolean true" | debconf-set-selections
echo "obs-api obs-api/dbconfig-reinstall boolean true" | debconf-set-selections
# Password of the database's administrative user:
echo "obs-api obs-api/mysql/admin-pass password ${DB_ROOT_PASSWORD}" | debconf-set-selections
# Password of the database's administrative user:
echo "dbconfig-common dbconfig-common/mysql/admin-pass password ${DB_ROOT_PASSWORD}" | debconf-set-selections
# MySQL application password for :
echo "dbconfig-common dbconfig-common/mysql/app-pass password ${DB_PASSWORD}" | debconf-set-selections
# Password confirmation:
echo "dbconfig-common dbconfig-common/app-password password ${DB_PASSWORD}" | debconf-set-selections
echo "dbconfig-common dbconfig-common/app-password-confirm password ${DB_PASSWORD}" | debconf-set-selections
# Password confirmation:
echo "dbconfig-common dbconfig-common/password password ${DB_PASSWORD}" | debconf-set-selections
echo "dbconfig-common dbconfig-common/password-confirm password ${DB_PASSWORD}" | debconf-set-selections
# Password confirmation:
echo "obs-api obs-api/app-password password ${DB_PASSWORD}" | debconf-set-selections
echo "obs-api obs-api/app-password-confirm password ${DB_PASSWORD}" | debconf-set-selections
# Password confirmation:
echo "obs-api obs-api/password password ${DB_PASSWORD}" | debconf-set-selections
echo "obs-api obs-api/password-confirm password ${DB_PASSWORD}" | debconf-set-selections
# MySQL application password for obs-api:
echo "obs-api obs-api/mysql/app-pass password ${DB_PASSWORD}" | debconf-set-selections
# Host running the MySQL server for obs-api:
echo "obs-api obs-api/remote/newhost string ${DB_HOST}" | debconf-set-selections
# Name of the database's administrative user:
echo "obs-api obs-api/mysql/admin-user string root" | debconf-set-selections
# Host name of the MySQL database server for obs-api:
# Choices:
echo "obs-api obs-api/remote/host select ${DB_HOST}" | debconf-set-selections
# Host name of the  database server for :
# Choices:
echo "dbconfig-common dbconfig-common/remote/host select ${DB_HOST}" | debconf-set-selections

# Port number for the MySQL service:
echo "obs-api obs-api/remote/port string 3306" | debconf-set-selections
# Connection method for MySQL database of obs-api:
# Choices: Unix socket, TCP/IP
echo "obs-api obs-api/mysql/method select Unix socket" | debconf-set-selections
# MySQL database name for obs-api:
echo "obs-api obs-api/db/dbname string ${DB_NAME}" | debconf-set-selections
# MySQL username for obs-api:
echo "obs-api obs-api/db/app-user string ${DB_USER}@${DB_NAME}" | debconf-set-selections

echo "mariadb-server-10.1 mysql-server/root_password password ${DB_ROOT_PASSWORD}" | debconf-set-selections
echo "mariadb-server-10.1 mysql-server/root_password_again password ${DB_ROOT_PASSWORD}" | debconf-set-selections

echo "Starting database server."
if [ ! -d "/var/lib/mysql/mysql" ]; then
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure mariadb-server-10.1
    service mysql start
    # Run dpkg-reconfigure twice to make sure it reconfigured correctly.
    echo "Configuring database ${DB_NAME} for ${DB_USER}@${DB_NAME}."
    DEBIAN_FRONTEND=noninteractive dpkg-reconfigure obs-api
else
    service mysql start
    cp /usr/share/obs/api/config/database.yml.example /etc/obs/api/config/database.yml
    sed -i s/"database: obsapi"/"database: ${DB_NAME}"/g /etc/obs/api/config/database.yml
    sed -i s/"username: obs-api"/"username: ${DB_USER}"/g /etc/obs/api/config/database.yml
    sed -i s/"password: _DBC_DBPASS_"/"password: ${DB_PASSWORD}"/g /etc/obs/api/config/database.yml
fi

if [ ! -z "$OBS_BACKEND_HOST" ]; then
    sed -i s/"source_host: localhost"/"source_host: ${OBS_BACKEND_HOST}"/g /etc/obs/api/config/options.yml
fi

service memcached start
echo "Setup rails app. Skip duplicate database error if database already exist."
/usr/share/obs/api/script/rake-tasks.sh setup
service obsapidelayed start
service apache2 restart

/usr/bin/supervisord -n
